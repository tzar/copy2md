chrome.runtime.onMessage.addListener(function(msg, sender, response){
  if(msg.request === "clip") {
    var fragment = window.getSelection().getRangeAt(0).cloneContents();
    var div = document.createElement("div");
    div.appendChild(fragment);
    response(div.innerHTML);
  }
});
