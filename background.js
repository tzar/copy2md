// For Context menu -> to markdown example
function copyTextToClipboard(text) {
  var copyFrom = document.createElement("textarea");
  copyFrom.textContent = text;
  var body = document.getElementsByTagName('body')[0];
  body.appendChild(copyFrom);
  copyFrom.select();
  document.execCommand('copy');
  body.removeChild(copyFrom);
}

chrome.runtime.onInstalled.addListener(function(){
  chrome.contextMenus.create({
    id: "42",
    title: "Copy as Markdown",
    contexts: ["selection"],
  });
});

chrome.contextMenus.onClicked.addListener(function(info, tab) {
  if(info.menuItemId != "42") return;
  chrome.tabs.sendMessage( tab.id, {request: "clip"}, function(selected_html) {
    if(typeof selected_html == "string") {
      copyTextToClipboard(toMarkdown(selected_html));
    }
  });
});
